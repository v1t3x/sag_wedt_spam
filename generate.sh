#!/bin/bash

RESDIR=results

for E in 1 0.8 0.6 0.4 0.2 0.1 0.05 0.01 0.005 0.001 
do
    for C in {1..10}
    do
    echo "./run.sh $C $E > $RESDIR/c${C}e${E}.out"
    ./run.sh $C $E | grep "dispatcher"  > $RESDIR/c${C}e${E}.out
    done

done

git add results/*
git commit -m "Results "
git push
