package pw.elka.agents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pw.elka.common.Constants;
import pw.elka.common.Mail;
import pw.elka.common.MailLabel;
import pw.elka.common.Utils;
import pw.elka.services.TrainingService;

public class TrainerAgent extends Agent {
	private static final long serialVersionUID = -6390345474920743112L;
	
	private static Logger logger = LoggerFactory.getLogger(TrainerAgent.class);
	
	private final static String TRAIN_SPAM_DIR = "/resources/trainSpam";
	private final static String TRAIN_NOT_SPAM_DIR = "/resources/trainNotSpam";
	
	private Random random = new Random();
	
	private List<Mail> mTrainingSpam = new ArrayList<Mail>();
	private List<Mail> mTrainingNotSpam = new ArrayList<Mail>();
	
	protected void setup() {
		mTrainingSpam.addAll(Utils.readMails(TRAIN_SPAM_DIR, MailLabel.SPAM));
		mTrainingNotSpam.addAll(Utils.readMails(TRAIN_NOT_SPAM_DIR, MailLabel.NOT_SPAM));
		
		Object [] args = getArguments();
		final double trainPortion = (double) args[0];
		
		addBehaviour(new TickerBehaviour(this,5000) {
			private static final long serialVersionUID = 228613810733802700L;

			public void onTick() {
				logger.trace("Trainer : Looking for classifiers to teach them");

				DFAgentDescription template = new TrainingService();
				DFAgentDescription[] results = null;
				try {
					results = DFService.search(myAgent, template);
				} catch (FIPAException e) {
					e.printStackTrace();
				}
				
				int noSpam = (int) (mTrainingSpam.size() * trainPortion);
				int noNotSpam = (int) (mTrainingNotSpam.size() * trainPortion);
				
				logger.trace("Training with "+noSpam+" spam and "+noNotSpam+" not spam messages");
				
				for(DFAgentDescription description : results) {
					AID aid = description.getName();
					
					logger.trace("Sending training mails to "+aid);
					
					//1. Send training mails

					List<Mail> trainingMails = new ArrayList<Mail>();
					
					for(int i=0;i<noSpam;++i) {
						int index = random.nextInt(mTrainingSpam.size());
						trainingMails.add(mTrainingSpam.get(index));
						mTrainingSpam.remove(index);
					}
					mTrainingSpam.addAll(trainingMails); //return them
					
					List<Mail> tmpNotSpam = new ArrayList<Mail>();
					for(int i=0;i<noNotSpam;++i) {
						int index = random.nextInt(mTrainingNotSpam.size());
						tmpNotSpam.add(mTrainingNotSpam.get(index));
						mTrainingNotSpam.remove(index);
					}
					trainingMails.addAll(tmpNotSpam);
					mTrainingNotSpam.addAll(tmpNotSpam); // return them
					
					ACLMessage trainingMsg = new ACLMessage(ACLMessage.INFORM);
					trainingMsg.addReceiver(aid);
					trainingMsg.setOntology(Constants.TRAINING_MAIL_ONTOLOGY);
					try {
						trainingMsg.setContentObject((Serializable) trainingMails);
					} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
					}
					send(trainingMsg);
					//2. Give signal to start learning
					
					ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
					msg.addReceiver(aid);
					msg.setOntology(Constants.TRAINING_ONTOLOGY);
					msg.setContent(Constants.LEARN);
					send(msg);
				}
			}
		} );

	}	
}
