package pw.elka.agents;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pw.elka.clasifier.IMailClsfr;
import pw.elka.clasifier.TfmClsfr;
import pw.elka.common.Constants;
import pw.elka.common.Mail;
import pw.elka.common.MailLabel;
import pw.elka.services.ClassifierService;
import pw.elka.services.TrainingService;

public class ClassifierAgent extends Agent {
	private static final long serialVersionUID = 7099238696094281905L;
	
	private static Logger logger = LoggerFactory.getLogger(ClassifierAgent.class);
	
	private IMailClsfr mClasifier = new TfmClsfr();
	
	protected void setup() {
		logger.trace("Seting up agent "+getAID().getName());
		DFAgentDescription dfd = new TrainingService();
		dfd.setName(getAID());

		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		
		addBehaviour(new CyclicBehaviour() {
			private static final long serialVersionUID = -7771832341809338587L;

			@Override
			public void action() {
				ACLMessage msg = myAgent.receive();
				if (msg!=null) {
					String ontology = msg.getOntology();
					String content = msg.getContent();
					
					switch(ontology) {
					case Constants.TRAINING_MAIL_ONTOLOGY:
						List<Mail> mails;
						try {
							mails = (List<Mail>) msg.getContentObject();
							mClasifier.learn(mails);
							logger.debug("Got "+mails.size()+" training mails.");
						} catch (UnreadableException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						break;
						
					case Constants.TRAINING_ONTOLOGY:
						logger.trace("Got training command: "+content);
						if (content.equals(Constants.LEARN)) {
							logger.trace("Will be learning");
							try {
								// Unregister from TrainingService
								DFService.deregister(myAgent);
								//Learn
								mClasifier.prepare();
								// Register as classifier
								DFAgentDescription dfd = new ClassifierService();//new DFAgentDescription();
								dfd.setName(getAID());
								DFService.register(myAgent, dfd);
								logger.trace("Learning completed");
							} catch (FIPAException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}
						break;
					case Constants.CLASIFING_ONTOLOGY:
						logger.debug("Got mail to clasify.");
						try {
							Mail mail = (Mail) msg.getContentObject();
							MailLabel clasification = mClasifier.classify(mail);
							ACLMessage reply = msg.createReply();
							reply.setContentObject(clasification);
							send(reply);
						} catch (UnreadableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// clasifier in wrong state
							e.printStackTrace();
						}
						
						break;
					default:
						logger.warn("Got message with unknown ontology");
						break;
					}
				} else {
					block();
				}
				
			}
		});
	}
	
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		
		logger.info("Terminated agent "+getAID().getName());
	}
}
