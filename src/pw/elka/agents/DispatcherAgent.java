package pw.elka.agents;

import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.JADEAgentManagement.JADEManagementOntology;
import jade.domain.JADEAgentManagement.ShutdownPlatform;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pw.elka.common.Constants;
import pw.elka.common.Mail;
import pw.elka.common.MailLabel;
import pw.elka.common.Utils;
import pw.elka.services.ClassifierService;

public class DispatcherAgent extends Agent {
	private static final long serialVersionUID = 8690238155933419336L;
	
	private static Logger logger = LoggerFactory.getLogger(DispatcherAgent.class);
	
	private final static String TEST_SPAM_DIR = "/resources/testSpam";
	private final static String TEST_NOT_SPAM_DIR = "/resources/testNotSpam";
	
	private List<Mail> mTestSet = new ArrayList<Mail>();
	
	private int TP = 0;
	private int FP = 0;
	private int TN = 0;
	private int FN = 0;
	
	
	protected void setup() {
		mTestSet.addAll(Utils.readMails(TEST_SPAM_DIR, MailLabel.SPAM));
		mTestSet.addAll(Utils.readMails(TEST_NOT_SPAM_DIR, MailLabel.NOT_SPAM));

		Object [] args = getArguments();
		final int classifiersCount = (int) args[0];
		
		addBehaviour(new WakerBehaviour(this,10000) {
			private static final long serialVersionUID = 228613810733802700L;

			public void handleElapsedTimeout() {
				logger.trace("Testing mails");

				logger.trace("Quorum size : "+classifiersCount);
				logger.trace("Waiting for "+classifiersCount+" clasifiers.");
				
				DFAgentDescription template = new ClassifierService();
				DFAgentDescription[] results = null;
				try {
					results = DFService.search(myAgent, template);
				} catch (FIPAException e) {
					e.printStackTrace();
				}
				
				logger.trace("Available clasifiers : "+results.length);
				
				if (results.length >= classifiersCount) {
					for(Mail testMail : mTestSet) {
						logger.trace("Testing mail : "+testMail);
						int sentQueries=0;
						int spam=0;
						int notSpam=0;		
						
						logger.trace("Sending mail to "+classifiersCount +" clasifiers");
						for(int i=0; i<classifiersCount; ++i) {
							// Choose agent //TODO remove duplications
							DFAgentDescription clsDescription = results[i];
							
							ACLMessage testMsg = new ACLMessage(ACLMessage.INFORM);
							testMsg.addReceiver(clsDescription.getName());
							testMsg.setOntology(Constants.CLASIFING_ONTOLOGY);
							try {
								testMsg.setContentObject(testMail);
							} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
							}
							send(testMsg);
							sentQueries++;
						}
						
						logger.trace("Waiting for replies");
						//TODO add timeout and so on
						
						while(spam+notSpam != sentQueries) {
							ACLMessage msg = myAgent.blockingReceive();
							if (msg!=null) {
								logger.trace("Received answer");
								if (msg.getOntology() == Constants.CLASIFING_ONTOLOGY) {
									try {
										MailLabel clasification = (MailLabel) msg.getContentObject();
										if (clasification.equals(MailLabel.SPAM)) {
											spam++;
										} else if(clasification.equals(MailLabel.NOT_SPAM)) {
											notSpam++;
										} else {
											logger.error("Received unknown type of mail label");
										}
									} catch (UnreadableException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									logger.error("Received unknown type of message");
								}
							} else {
								logger.error("msg is null");
							}
							
							if (spam+notSpam == sentQueries) {
								MailLabel classification = (spam>notSpam?MailLabel.SPAM:MailLabel.NOT_SPAM); // TODO oder other types of answer / kinds of voting
								logger.debug("Clasification: "+classification+" (should be "+testMail.Label+")");
								
								if(classification.equals(MailLabel.SPAM)) {//clasified as SPAM
									if(testMail.Label.equals(MailLabel.SPAM)) {
										TP++;
									} else {
										FP++;
									}
								} else { //classified ans NOT_SPAM
									if(testMail.Label.equals(MailLabel.SPAM)) {
										FN++;
									} else {
										TN++;
									}
								}
							}
						}	
					}
					logger.info("=====RESULTS=====");
					
					logger.info("TP= "+TP);
					logger.info("FP= "+FP);
					logger.info("TN= "+TN);
					logger.info("FN= "+FN);
					
					logger.info("Precison= "+( (double)TP/(TP+FP)) );
					logger.info("Accuracy= "+( (double)(TP+TN)/(TP+FP+TN+FN)) );
					logger.info("Fallout=  "+( (double)Math.abs(FP-FN)/(TN+FP)) );
					logger.info("Recall=   "+( (double)TP/(TP+FN)) );
					
					
			 		Codec codec = new SLCodec();    
					Ontology jmo = JADEManagementOntology.getInstance();
					getContentManager().registerLanguage(codec);
					getContentManager().registerOntology(jmo);
					ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
					msg.addReceiver(getAMS());
					msg.setLanguage(codec.getName());
					msg.setOntology(jmo.getName());
					try {
					    getContentManager().fillContent(msg, new Action(getAID(), new ShutdownPlatform()));
					    send(msg);
					}
					catch (Exception e) {}
					 
					
				} else {
					// Reset behaviour
					logger.info("Not enough classifiers. Reseting behaviour");
					reset();
				}
			}
		} );

	}
}
