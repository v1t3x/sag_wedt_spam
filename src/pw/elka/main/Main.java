package pw.elka.main;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;

public class Main {
	private static Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
		root.setLevel(Level.INFO);

		Runtime rt = Runtime.instance();
		Profile p = new ProfileImpl();
		p.setParameter("gui", "false");
		
		if (args.length < 2) {
			logger.error("Usage : run c e");
			logger.error("c - classifiers number");
			logger.error("e - percent of mails to be sent for teaching");
			return;
		}
		
		int cN = Integer.parseInt(args[0]);
		double eN = Double.parseDouble(args[1]);
		
		logger.debug("c="+cN+" e="+eN);
		
		ContainerController cc = rt.createMainContainer(p);
		
		logger.info("Creating Dispatcher agent ...");
		try {
			Object [] dArgs = new Object[1];
			dArgs[0]=cN;
			AgentController dispatcher = cc.createNewAgent("dispatcher", "pw.elka.agents.DispatcherAgent", dArgs);
			dispatcher.start();
		} catch (StaleProxyException e) {
			logger.error("Could not start dispatcher agent.");
			e.printStackTrace();
		} 
		
		logger.info("Creating Trainer agent ...");
		try {
			Object [] tArgs = new Object[1];
			tArgs[0]=eN;
			AgentController dispatcher = cc.createNewAgent("trainer", "pw.elka.agents.TrainerAgent", tArgs);
			dispatcher.start();
		} catch (StaleProxyException e) {
			logger.error("Could not start dispatcher agent.");
			e.printStackTrace();
		} 
		
		logger.info("Creating Classifier agents ...");		
		for(int i=1; i<=cN; ++i) {
			try {
				AgentController classifier = cc.createNewAgent("classifier_"+i, "pw.elka.agents.ClassifierAgent", null);
				classifier.start();
			} catch (StaleProxyException e) {
				logger.error("Could not start classifier agent number "+i);
				e.printStackTrace();
			}
		
		}		
	}

}
