package pw.elka.common;

public class Constants {
	public final static int CLASSIFIERS = 5;
	
	public final static String TRAINING_MAIL_ONTOLOGY = "TrainingMail";
	public final static String TRAINING_ONTOLOGY = "Training";
	public final static String CLASIFING_ONTOLOGY = "Clasify";
	
	public final static String LEARN = "Learn";
}
