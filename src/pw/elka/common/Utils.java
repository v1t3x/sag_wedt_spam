package pw.elka.common;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Utils {
	
	public static Collection<Mail> readMails(String dir, MailLabel label) {
		List<Mail> mails = new ArrayList<Mail>();
		
		URL url = Utils.class.getClass().getResource(dir);
        File folder;
		try {
			folder = new File(url.toURI());
	        File[] listOfFiles = folder.listFiles();
	        for (File file : listOfFiles) {
	            if (file.isFile()) {
	                try {
	                    String text = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
	                    mails.add(new Mail(label, text));
	                } catch (Exception e) {
	                }
	            }
	        }
		} catch (URISyntaxException e1) {
			System.out.println("ERROR : Unable to load mails from "+dir);
		}
        return mails;
	}

}
