/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pw.elka.common;

import java.io.Serializable;

/**
 *
 * @author Przemysław
 */
public class Mail implements Serializable {
	private static final long serialVersionUID = -2722893524962189035L;
	public Mail(MailLabel label, String text){
        Label = label;
        Text = text;
    }
    
    public MailLabel Label;
    public String Text;    
}
