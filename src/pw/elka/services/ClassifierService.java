package pw.elka.services;

import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class ClassifierService extends DFAgentDescription {
	private static final long serialVersionUID = 6976094781415684011L;

	public ClassifierService() {
		super();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("classifiers");
		sd.setName("classifier");
		addServices(sd);
	}
}
