package pw.elka.services;

import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class TrainingService extends DFAgentDescription {
	private static final long serialVersionUID = 3610918208830638887L;

	public TrainingService() {
		super();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("classifiers");
		sd.setName("training");
		addServices(sd);
	}
}
