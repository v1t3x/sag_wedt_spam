/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pw.elka.clasifier;
import java.util.*;
import java.util.Map;
/**
 *
 * @author Przemysław
 */
public class DictComparator implements Comparator<Map.Entry<String, Long>>{
    public int compare(Map.Entry<String, Long> a, Map.Entry<String,Long> b) {
        if (a.getValue() > b.getValue()) {
            return -1;
        } else if(a.getValue() < b.getValue()){
            return 1;
        } else{
            return 0;
        }// returning 0 would merge keys
    }
}
