/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw.elka.clasifier;

import java.util.*;

import pw.elka.common.Mail;
import pw.elka.common.MailLabel;
import pw.elka.common.VoteType;

/**
 *
 * @author Przemysław
 */
public class TfmClsfr implements IMailClsfr {

    public VoteType Type = VoteType.Plurality;
    public int K = 1;

    protected List<Mail> mails = new ArrayList<>();
    protected Tokenizer tokenizer = new Tokenizer();
    protected double trimTop = 15.0; // percentage of most popular tokens to be skipped
    protected double trimBot = 15.0; //  percentage of least popular tokens to be skipped
    protected Metric metric = new Metric();

    protected boolean prepared = false;    
    protected List<String> words;
    protected double[] wordsFactors;
    protected Map<String, Integer> hashedWords;
    protected double[][] tfm;
    protected MailLabel[] labels;

    // append new mails to training set
    public void learn(List<Mail> ms) {
        mails.addAll(ms);
    }

    // clears training set
    public void forget() {
        mails.clear();
    }

    // must be called after changing training set to commit this changes to classification results
    public void prepare() {
        // todo: clearing all members here

        // filling dictionary
        Map<String, Long> dictionary = new HashMap<>();
        List<List<String>> tokenized = new ArrayList<>();
        for (int i = 0; i < mails.size(); ++i) {
            List<String> tokens = tokenizer.Tokenize(mails.get(i).Text);
            tokenized.add(tokens);
            for (int j = 0; j < tokens.size(); ++j) {
                Long oldNum = dictionary.get(tokens.get(j));
                dictionary.put(tokens.get(j), (oldNum == null ? 1 : oldNum + 1));
            }
        }

        // trimming dictionary
        DictComparator dictComp = new DictComparator();
        List<Map.Entry<String, Long>> tmpWords = new ArrayList<>(dictionary.entrySet());
        Collections.sort(tmpWords, dictComp);
        tmpWords = tmpWords.subList(
                (int) (trimBot / 100.0 * (tmpWords.size() - 1)),
                (int) ((100.0 - trimTop) / 100.0 * (tmpWords.size() - 1))
        );
        hashedWords = new HashMap<>();
        for (int i = 0; i < tmpWords.size(); ++i) {
            hashedWords.put(tmpWords.get(i).getKey(), i);
        }
        words = new ArrayList<>();
        for (int i = 0; i < tmpWords.size(); ++i) {
            words.add(tmpWords.get(i).getKey());
        }

        // creating and filling tfm and labels
        tfm = new double[mails.size()][tmpWords.size()];
        labels = new MailLabel[mails.size()];        
        for (int i = 0; i < mails.size(); ++i) {
            labels[i] = mails.get(i).Label;
            for (int j = 0; j < tokenized.get(i).size(); ++j) {
                Integer col = hashedWords.get(tokenized.get(i).get(j));
                if (col != null) {
                    ++tfm[i][col];
                }
            }
        }

        // tfm matrix transformations (http://pl.wikipedia.org/wiki/TFIDF)
        // TF
        wordsFactors = new double[tmpWords.size()];
        for (int i = 0; i < mails.size(); ++i) {
            long wordsNumInDocument = 0;
            for (int j = 0; j < tmpWords.size(); ++j) {
                wordsNumInDocument += tfm[i][j];
            }
            for (int j = 0; j < tmpWords.size(); ++j) {
                tfm[i][j] /= wordsNumInDocument;
            }
        }
        //IDF
        for (int j = 0; j < tmpWords.size(); ++j) {
            long DocumentsNumWithGivenToken = 0;
            for (int i = 0; i < mails.size(); ++i) {
                DocumentsNumWithGivenToken += tfm[i][j] > 0 ? 1 : 0;
            }
            for (int i = 0; i < mails.size(); ++i) {
                double wordFactor = Math.log10(mails.size() / DocumentsNumWithGivenToken);
                wordsFactors[j] = wordFactor;
                tfm[i][j] *= wordFactor;
            }
        }

        prepared = true;
    }

    private double[] toWordsSpace(String mail) {
        double[] res = new double[words.size()];
        
        List<String> tokenized = tokenizer.Tokenize(mail);
        for (int i = 0; i < tokenized.size(); ++i) {
            Integer ind = hashedWords.get(tokenized.get(i));
            if (ind != null) {
                ++res[ind];
            }
        }
        
        long sum = 0;
        for (int i = 0; i < words.size(); ++i) {
            sum += res[i];            
        }
        for (int i = 0; i < words.size(); ++i) {
            res[i] /= sum;            
        }
        
        for (int i = 0; i < words.size(); ++i) {
            res[i] *= wordsFactors[i];            
        }
        
        return res;
    }

    // classifies List of mails
    public List<MailLabel> classify(List<String> toClassify) throws Exception {
        if (!prepared) {
            throw new Exception("not prepared");
        }

        List<MailLabel> res = new ArrayList<>(mails.size());

        for (int i = 0; i < toClassify.size(); ++i) {
            SortedMap<Double, Mail> tmp = new TreeMap<>();
            double[] inWordSpace = toWordsSpace(toClassify.get(i));
            for (int j = 0; j < mails.size(); ++j) {
                tmp.put(metric.Dist(tfm[j], inWordSpace), mails.get(j));
            }
            int spamCounter = 0;
            int counter = 0;
            for (Map.Entry<Double, Mail> e : tmp.entrySet()) {
                if (counter >= K) {
                    break;
                }
                spamCounter += e.getValue().Label == MailLabel.SPAM ? 1 : 0;
                ++counter;
            }
            if (Type == VoteType.Unaminity) {
                if (spamCounter == 0) {
                    res.add(MailLabel.NOT_SPAM);
                } else if (spamCounter == K) {
                    res.add(MailLabel.SPAM);
                } else {
                    res.add(MailLabel.REJECTED);
                }
            } else {
                if (spamCounter > K - spamCounter) {
                    res.add(MailLabel.SPAM);
                } else if (K - spamCounter > spamCounter) {
                    res.add(MailLabel.NOT_SPAM);
                } else {
                    res.add(MailLabel.REJECTED);
                }
            }

        }

        return res;
    }

	@Override
	public MailLabel classify(Mail mail) throws Exception {
        if (!prepared) {
            throw new Exception("not prepared");
        }

        SortedMap<Double, Mail> tmp = new TreeMap<>();
        double[] inWordSpace = toWordsSpace(mail.Text);
        for (int j = 0; j < mails.size(); ++j) {
            tmp.put(metric.Dist(tfm[j], inWordSpace), mails.get(j));
        }
        int spamCounter = 0;
        int counter = 0;
        for (Map.Entry<Double, Mail> e : tmp.entrySet()) {
            if (counter >= K) {
                break;
            }
            spamCounter += e.getValue().Label == MailLabel.SPAM ? 1 : 0;
            ++counter;
        }
        if (Type == VoteType.Unaminity) {
            if (spamCounter == 0) {
                return MailLabel.NOT_SPAM;
            } else if (spamCounter == K) {
                return MailLabel.SPAM;
            } else {
                return MailLabel.REJECTED;
            }
        } else {
            if (spamCounter > K - spamCounter) {
                return MailLabel.SPAM;
            } else if (K - spamCounter > spamCounter) {
                return MailLabel.NOT_SPAM;
            } else {
                return MailLabel.REJECTED;
            }
        }
	}
}
