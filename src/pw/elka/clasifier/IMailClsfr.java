/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pw.elka.clasifier;
import java.util.*;

import pw.elka.common.Mail;
import pw.elka.common.MailLabel;

/**
 *
 * @author Przemysław
 */
public interface IMailClsfr {
    // append new mails to training set
    void learn(List<Mail> mails);
    
    // clears training set
    void forget();
    
    // must be called after changing training set to commit this changes to classification results
    void prepare() ;
    
    // classifies List of mails
    List<MailLabel> classify(List<String> mails) throws Exception;
    
    // classifies mail
    MailLabel classify(Mail mail) throws Exception;
    
}
