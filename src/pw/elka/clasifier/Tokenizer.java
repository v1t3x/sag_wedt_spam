/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pw.elka.clasifier;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Przemysław
 */
public class Tokenizer {
    public List<String> Tokenize(String text){
        text.toLowerCase();
        
        //[a-z0-9]{1}[a-z0-9_.\\\\-@]*[a-z]+[a-z0-9_.\\\\-@]*[a-z0-9]{1}
        Pattern p = Pattern.compile("[a-z0-9]{1}[a-z0-9_.-@]*[a-z]+[a-z0-9_.-@]*[a-z0-9]{1}");
        Matcher m = p.matcher(text);
        
        List<String> list = new ArrayList<String>();
        
        while(m.find()){
            list.add(m.group());
        }
        
        return list;
    }
}
