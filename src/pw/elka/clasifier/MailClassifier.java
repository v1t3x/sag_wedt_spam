/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw.elka.clasifier;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.util.*;

import pw.elka.common.Mail;
import pw.elka.common.MailLabel;

/**
 *
 * @author Przemysław
 */
public class MailClassifier {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String trainSpam = "D:\\Download\\R\\trainSpam";
        String trainNotSpam = "D:\\Download\\R\\trainNotSpam";
        String testSpam = "D:\\Download\\R\\testSpam";
        String testNotSpam = "D:\\Download\\R\\testNotSpam";

        List<Mail> mailSpam = new ArrayList<>();
        List<Mail> mailNotSpam = new ArrayList<>();
        List<String> mailTestSpam = new ArrayList<>();
        List<String> mailTestNotSpam = new ArrayList<>();

        TfmClsfr classifier = new TfmClsfr();
        

        File folder = new File(trainSpam);
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                try {
                    String text = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
                    mailSpam.add(new Mail(MailLabel.SPAM, text));
                } catch (Exception e) {
                }
            }
        }

        folder = new File(trainNotSpam);
        listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                try {
                    String text = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
                    mailNotSpam.add(new Mail(MailLabel.NOT_SPAM, text));
                } catch (Exception e) {
                }
            }
        }

        folder = new File(testSpam);
        listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                try {
                    String text = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
                    mailTestSpam.add(text);
                } catch (Exception e) {
                }
            }
        }

        folder = new File(testNotSpam);
        listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                try {
                    String text = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())), StandardCharsets.UTF_8);
                    mailTestNotSpam.add(text);
                } catch (Exception e) {
                }
            }
        }

        classifier.learn(mailSpam);
        classifier.learn(mailNotSpam);
        classifier.prepare();
        List<MailLabel> spamRes = new ArrayList<MailLabel>();
        List<MailLabel> notSpamRes = new ArrayList<MailLabel>();

        try {
            spamRes = classifier.classify(mailTestSpam);
            notSpamRes = classifier.classify(mailTestNotSpam);
        } catch (Exception e) {
        }

        int spamCountInRes = 0;
        int notSpamCountInRes = 0;

        for (int i = 0; i < spamRes.size(); ++i) {
            if (spamRes.get(i) == MailLabel.SPAM) {
                ++spamCountInRes;
            }
        }
        
        for (int i = 0; i < notSpamRes.size(); ++i) {
            if (notSpamRes.get(i) == MailLabel.NOT_SPAM) {
                ++notSpamCountInRes;
            }
        }
        System.out.print("wynik:\n" 
                + Integer.toString(spamCountInRes) + "/" + spamRes.size() + " (spam)\n"
                + Integer.toString(notSpamCountInRes) + "/" + notSpamRes.size() + " (not spam)\n");

        //String s = console.readLine(to);
        //console.printf(s);
    }

}
