/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw.elka.clasifier;

/**
 *
 * @author Przemysław
 */
public class Metric {

    public double Dist(double[] a, double[] b) throws Exception {
        if (a.length != b.length) {
            throw new Exception("diffent number of dimensions");
        }
        
        double lenOfA = 0;
        double lenOfB = 0;
        double dotProduct = 0;
        for(int i=0; i<a.length; ++i){
            lenOfA += a[i]*a[i];
            lenOfB += b[i]*b[i];
            dotProduct += a[i]*b[i];                   
        }
        lenOfA = Math.sqrt(lenOfA);
        lenOfB = Math.sqrt(lenOfB);
        double cos = dotProduct/(lenOfA*lenOfB);
        return Math.abs(cos-1);
    }
}
